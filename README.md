# Custom Bluescreen

This is a custom Windows Bluescreen.
The font used is [Segoe UI Variable](https://aka.ms/SegoeUIVariable) which is the actual Windows 11 font.
If this is not available it falls back to [Selawik](https://github.com/winjs/winstrap/tree/master/src/fonts) which aims to be similar.

![preview of the bluescreen](./screen.svg)
